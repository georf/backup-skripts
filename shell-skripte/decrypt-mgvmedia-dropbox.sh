#!/bin/bash

MOUNTPOINT=~/MGVmedia

# ask for encrypt
function ready {
  sleep 1
  kdialog --yesnocancel 'Bist du fertig mit der Arbeit?'
  case "$?" in
    0)
      encrypt
      ;;
    1)
      ready
      ;;
    *)
      exit 0
      ;;
  esac
}

function encrypt {
  fusermount -u $MOUNTPOINT
  case "$?" in
    0)
      rmdir $MOUNTPOINT
      exit 0
      ;;
    *)
      kdialog --error "Ein Programm blockiert"
      ready
      ;;
  esac
}

function decrypt {
  # decrypt directory with password from kdialog
  encfs -i 1 --extpass="kdialog --password Passwort?" ~/Dropbox/.MGVmedia ~/MGVmedia
  case "$?" in
    0)
      # wait for user interaction
      ready
      ;;
    *)
      kdialog --error "Vermutlich ein falsches Passwort"
      decrypt
      ;;
  esac
}



if [ -d $MOUNTPOINT ]
then
  kdialog --msgbox "Das Verzeichnis $MOUNTPOINT existiert bereits."
  exit 1;
fi

# create mountpoint
mkdir $MOUNTPOINT

decrypt