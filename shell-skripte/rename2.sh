#!/bin/bash
dir="test/";

if ! test -e $dir 
then 
	mkdir $dir
fi

for file in *.JPG
do
picdate="`exiftool -CreateDate -d \"%Y-%m-%d_%H.%M.%S\" "$file"  | cut -f2 -d: | tr -d '=' | tr -d ' ' `"
echo $picdate
if test -e "$dir$picdate.JPG"
	then {
		n=1;
		while test -e "$dir$picdate($n).JPG"
		do
			let "n++";
		done
		cp "$file" "$dir$picdate($n).JPG"
	}
else 
	cp "$file" "$dir$picdate.JPG";
fi
done
