#!/bin/bash


if [ ! -d output ] ; then mkdir output ; fi 

for file in *.AVI
do
	/usr/bin/ffmpeg -y -i "$file" -f mp4 -r 25 -threads 2 -vcodec libx264 -vpre medium -b 1000k -flags +loop -cmp +chroma -deblockalpha 0 -deblockbeta 0 -b 1250k -maxrate 1500k -bufsize 4M -bt 256k -refs 1 -bf 3 -coder 1 -me_method umh -me_range 16 -subq 7 -partitions +parti4x4+parti8x8+partp8x8+partb8x8 -g 250 -keyint_min 25 -level 30 -qmin 10 -qmax 51 -qcomp 0.6 -trellis 2 -sc_threshold 40 -i_qfactor 0.71 -acodec libvo_aacenc -ab 64k -ar 44100 -ac 2 "output/$file.mp4"
done
