<?php

require_once('mail/class.phpmailer.php');

$content = file('leute.csv');
foreach ($content as $line) {
    $result = str_getcsv($line);

    // Nachname	Vorname	Adresse	PLZ	Ort	Telefon	Handy	Geburtstag	E-Mail	PKW-Kennzeichen	FFW	Ausweisnummer	Geburstag sichtbar
    list($nr, $name, $firstname, $address, $plz, $city, $home, $mobile, $birthday, $email, $pkw, $ffw, $ausweis, $show) = $result;

    $text = 'Hallo '.$firstname.",\n";

    $text .= "\n";
    $text .= "ich bin gerade beim Aufbereiten der Benutzerdaten für das Team MV. Dafür bitte ich dich, die nachfolgenden Daten auf Korrektheit und Vollständigkeit zu überprüfen. Sollte etwas fehlen oder nicht mehr aktuell sein, schickst du mir einfach eine E-Mail mit den neuen Daten zu.\n";
    $text .= "\n";
    $text .= "Bei den Telefonnummern und Ausweisnummern habe ich die ersten 6 Zeichen verdeckt, damit dein E-Mail-Anbieter die nicht mitlesen kann. Hier kannst du anhand der letzten Zeichen die Korrektheit überprüfen.\n";
    $text .= "\n";
    $text .= check("Name", $name)."\n";
    $text .= check("Vorname", $firstname)."\n";
    $text .= check("Straße", $address)."\n";
    $text .= check("PLZ", $plz)."\n";
    $text .= check("Stadt", $city)."\n";
    $text .= check("Festnetz", $home, true)."\n";
    $text .= check("Handy", $mobile, true)."\n";
    $text .= check("Geburtstag", $birthday)."\n";
    $text .= check("Kennzeichen", $pkw)."\n";
    $text .= check("FF", $ffw)."\n";
    $text .= check("Ausweisnummer", $ausweis, true)."\n";
    $text .= "Dein Geburtstag ist für Team-Mitglieder sichtbar: ".(($show == "1")? "Ja":"Nein");

    $text .= "\n\n";
    $text .= "Es sollte wenigstens eine funktionierende Telefonnummer hinterlegt werden, falls wir mal was dringendes haben. Das Kennzeichen und die Ausweisnummer brauchen wir für die Teilnahme an den Trainingslagern in Böhlendorf (Bundeswehr). Deine Anschrift wird eventuell bei diversen Anmeldungen gebraucht (Wettkämpfe und co; nicht für Werbung).\n";
    $text .= "\n";
    $text .= "In den Anhang habe ich noch ein Dokument mit vielen Informationen zu den Online-Diensten vom Team gelegt. Bitte lies dir das mal durch. Das meiste sollte eigentlich bekannt sein. Wenn du Fragen dazu hast, bitte einfach melden.\n\n";
    $text .= "Jetzt noch einen schönen Sonntag. Liebe Grüße\nGeorg\n\n";


    $mail = new PHPMailer();
    $mail->CharSet = 'utf-8';
    $mail->SetLanguage ("de");

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'localhost';  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = '';                            // SMTP username
$mail->Password = '';                           // SMTP password
$mail->SMTPSecure = 'tls';

    $mail->SetFrom('georf@georf.de', 'Georg Limbach');

    $address = $email;
    $mail->AddAddress($address, $firstname.' '.$name);
    $mail->addAttachment(__DIR__."/Dienste.pdf");

    $mail->Subject    = "Team-MV - Deine Daten aktuell?";

    $mail->Body    = $text;

    if(!$mail->Send()) {
      echo "Mailer Error: " . $mail->ErrorInfo."\n";
    } else {
      echo "Message sent!\n";
    }
}

function check($name, $content, $escape = false) {
    $content = trim($content);
    if (empty($content)) {
        return $name.": Hast du noch nicht angegeben";
    } else {
        return $name.": ".($escape? escape($content):$content);
    }
}

function escape($string) {
    return preg_replace('|^.{6}(.+)$|', 'XXXXXX$1', $string);
}
