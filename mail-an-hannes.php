#!/usr/bin/php
<?php

$betreff = array(
    "Annes Stick",
    "Hast du schon Annes Stick?",
    "Denkst du dran?",
    "Stick von Anne",
    "Anne vermisst was ...",
    "Wann denn nun?",
);

$anrede = array(
    "Hallo Hannes",
    "Hallo",
    "Moin",
    "Hey",
    "Moin Hannes",
    "Wie gehts?",
    "",
);

$text1 = array(
    "Anne vermisst ihren Stick.",
    "Der Stick von Anne ist immer noch nicht da.",
    "Wie lange braucht dein Arbeitskollege den Stick noch?",
    "Anne würde den Stick gerne wiederhaben."
);


$text2 = array(
    "Hast du schon was erreicht?",
    "Schreibe ihm am besten gleich eine SMS.",
    "Fragst du ihn morgen gleich mal?",
    "Anne braucht den Stick für die Uni.",
    "Sie vermisst ihn wirklich.",
);

$abschluss = array(
    "Bis dann",
    "Hau rein",
    "LG Georg",
    "LG Georg & Anne",
    "LG",
    "Lieben Gruß",
    "Lieben Gruß\nGeorg",
);

echo $betreff[array_rand($betreff)]."\n\n";

echo $anrede[array_rand($anrede)]."\n\n";
echo $text1[array_rand($text1)]." ";
echo $text2[array_rand($text2)]."\n\n";
echo $abschluss[array_rand($abschluss)]."\n";
